package com.example.weatherapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter(var cityList: ArrayList<City>) :
    RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val viewItem =
            LayoutInflater.from(parent.context).inflate(R.layout.weather_item, parent, false)
        return RecyclerViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bind(cityList[position])
    }


    class RecyclerViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(city: City) {
            view.findViewById<TextView>(R.id.city_text).text = city.name
            view.findViewById<TextView>(R.id.temperature_text).text = "${city.temperature} ℉"
            view.findViewById<TextView>(R.id.pressure_text).text = city.pressure.toString()
            view.findViewById<TextView>(R.id.humidity_text).text = city.humidity.toString()
        }
    }
}