package com.example.weatherapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var viewManger: RecyclerView.LayoutManager
    lateinit var viewAdapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewManger = LinearLayoutManager(this)
        viewAdapter = RecyclerViewAdapter(ArrayList())
        weather_recyclerView.layoutManager = viewManger
        weather_recyclerView.adapter = viewAdapter

        add_button.setOnClickListener {
        }

        clear_button.setOnClickListener {
        }
    }
}